## TelegramTools.NET

Useful tools to help you bootstrap your Telegram bot

<br/>

### Getting Started

Clone the project and also the Telegram.Bot project: 

https://github.com/TelegramBots/Telegram.Bot

Build Telegram.Bot and add Telegram.Bot.dll to the References of TelegramTools project and build.

Add TelegramTools.dll to your project's References and in your code as:

`using TelegramBotTools;`

Create a bot by talking to BotFather on Telegram. Once you have the API token, run a program like this:

`new TelegramBot(YOUR_BOT_NAME, YOUR_BOT_API_TOKEN).Run();`

and you will be able to talk to your bot.

<br/>

### Documentation

Restrict the bot to only respond to certain people:

`var bot = new TelegramBot(YOUR_BOT_NAME, YOUR_BOT_API_TOKEN, [USER1_ID, USER2_ID]);`

Add a bot command by adding a method in a derived class:

```
public class MyTelegramBot : TelegramBot
{
    [TelegramBotCommand("mycommand", "A command added as method", "(-r) - A required parameter\n(-o) - An optional parameter")]
    public string MyCommand(string r, string o = null)
    {
        // Do something
        
        return "My response";
    }
}
```

Add a bot command dynamically to a TelegramBot object:

```
var bot = new TelegramBot(YOUR_BOT_NAME, YOUR_BOT_API_TOKEN);
bot.AdditionalCommands.Add(new TelegramBotAdditionalCommand("mycommand", "A command added dynamically", ParamSettings){
    Func = (args)=> {
        // Do something
        
        return "My response";
    };
});
```
The `ParamSettings` above is a **Dictionary<string, bool>** contains all the parameters for this command. The boolean value means if the parameter is optional.

<br/>

#### Prerequisites

Visual Studio 2017+


#### Language

C#


#### Author

Caveman


#### License

This project is licensed under the MIT License - see the LICENSE file for details