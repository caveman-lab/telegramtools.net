﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBotTools
{
    public class TelegramBotCommand : Attribute
    {
        internal static readonly Type SendMessageFuncType = typeof(Func<string, Task<Message>>);

        private string _command;
        private string _description;
        private string _argUsage;
        public string Command { get => _command; }
        public string Description { get => _description; }
        public string ArgUsage { get => _argUsage; }

        public TelegramBotCommand(string command, string description, string argUsage = null)
        {
            this._command = command.ToLower();
            this._description = description;
            this._argUsage = argUsage;
        }
    }

    public class TelegramBotAdditionalCommand
    {
        private readonly TelegramBotCommand _attr;
        private readonly Dictionary<string, bool> _settings;  // <Name, IsOptional>
        public TelegramBotCommand Attribute { get => _attr; }
        public Dictionary<string, bool> Settings { get => _settings; }

        public TelegramBotAdditionalCommand(TelegramBotCommand attr)
        {
            this._attr = attr;
        }

        public TelegramBotAdditionalCommand(TelegramBotCommand attr, Dictionary<string, bool> settings)
        {
            this._attr = attr;
            this._settings = settings;
        }

        public Func<Dictionary<string, string>, string> Func;  // <Name, Value>
    }

    public class TelegramBot : IDisposable
    {
        private TelegramBotClient Client;
        private readonly string _apiToken;
        private long[] _allowedUsers;
        private string _name;

        protected string Name { get => _name; }
        protected long[] AllowedUsers { get => _allowedUsers; }

        public TelegramBot(string name, string apiToken)
        {
            this._name = name;
            this._apiToken = apiToken;
            this.Client = new Telegram.Bot.TelegramBotClient(apiToken);

            InitCommands();
        }

        public TelegramBot(string name, string api, long[] allowedUsers) : this(name, api)
        {
            this._allowedUsers = allowedUsers;
        }

        public Task<Message> SendTextMessageAsync(long chatId, string msg)
        {
            return Client.SendTextMessageAsync(chatId, msg, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }

        public Task<Message> SendTextMessageAsync(string chatId, string msg)
        {
            return Client.SendTextMessageAsync(chatId, msg, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }

        public void Run()
        {
            Client.OnMessage += (c, args) =>
            {
                if (_allowedUsers == null || _allowedUsers.Length == 0 || _allowedUsers.Contains((args?.Message?.From?.Id ?? 0)))
                {
                    ProcessMessageAsync(args.Message.Text, args.Message.Chat.Id);
                }
            };

            Client.StartReceiving();
        }

        private async Task ProcessMessageAsync(string text, long chatId)
        {
            await new TaskFactory().StartNew(() =>
            {
                try
                {
                    var result = ExecuteCommand(text, chatId);
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                        SendTextMessageAsync(chatId, result);
                    }
                }
                catch (Exception ex)
                {
                    SendTextMessageAsync(chatId, $"(*Error*) Please report to admin:\n{ex.Message}");
                }
            });
        }

        #region Commands
        private Dictionary<string, MethodInfo> Commands = new Dictionary<string, MethodInfo>();
        private void InitCommands()
        {
            var cmds = this.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => { var cmd = m.GetCustomAttribute<TelegramBotCommand>(); return cmd != null; })
                .Where(m => m.ReturnType == typeof(string));

            foreach (var cmd in cmds)
            {
                var attr = cmd.GetCustomAttribute<TelegramBotCommand>();
                if (!string.IsNullOrWhiteSpace(attr.Command) && !Commands.ContainsKey(attr.Command))
                {
                    Commands.Add(attr.Command, cmd);
                }
            }
        }
        public List<TelegramBotAdditionalCommand> AdditionalCommands = new List<TelegramBotAdditionalCommand>();

        [TelegramBotCommand("start", "Start a new conversation")]
        public string Start()
        {
            return $"Hi, this is {_name}! Please use /help to see what you can do with me.";
        }

        [TelegramBotCommand("help", "Check commands", "(-c) - Optional parameter to check the detail of a single command")]
        public string Help(string c = null)
        {
            if (!string.IsNullOrWhiteSpace(c))
            {
                c = c.Trim().ToLower();
                if (c.StartsWith("/")) c = c.Substring(1);
                var detail = GetCommandDetail(c);
                if (!string.IsNullOrWhiteSpace(detail)) return detail;
            }

            var cmds = Commands.Where(_cmd => _cmd.Key != "start")
                .Select(_cmd =>
                {
                    var m = _cmd.Value;
                    var attr = m.GetCustomAttribute<TelegramBotCommand>();
                    var args = m.GetParameters() ?? new ParameterInfo[0];
                    return $"/{attr.Command} {string.Join("", args.Select(a => a.IsOptional ? $"(-{a.Name}) " : $"\\[-{a.Name}] "))}- {attr.Description}";
                });
            var addi_cmds = AdditionalCommands?.Where(m => !string.IsNullOrWhiteSpace(m.Attribute.Command) && m.Func != null).Select(m =>
            {
                var cmd = m.Attribute;
                var args = m.Settings ?? new Dictionary<string, bool>();
                return $"/{cmd.Command} {string.Join("", args.Select(a => a.Value ? $"(-{a.Key}) " : $"\\[-{a.Key}] "))}- {cmd.Description}";
            });

            var msg = string.Empty;

            msg = msg.AppendNewLine("/{command} \\[-{required}] (-{optional}) - {description}");
            msg = msg.AppendNewLine();

            msg = msg.AppendNewLine(string.Join(Environment.NewLine, cmds));
            msg = msg.AppendNewLine(string.Join(Environment.NewLine, addi_cmds));
            msg = msg.AppendNewLine();

            msg = msg.AppendNewLine("*Example:*");
            msg = msg.AppendNewLine("/{command} -a _aValue_ -b -c _cValue_");
            msg = msg.AppendNewLine();

            msg = msg.AppendNewLine("*For more detail:*");
            msg = msg.AppendNewLine("/help -c {command}");
            msg = msg.AppendNewLine("/{command} -help");
            msg = msg.AppendNewLine();

            return msg;
        }

        private string GetCommandDetail(string command)
        {
            // Native
            if (Commands.ContainsKey(command))
            {
                return GetCommandDetail(Commands[command].GetCustomAttribute<TelegramBotCommand>(), Commands[command].GetParameters().ToDictionary(t => t.Name, t => t.IsOptional));
            }
            // Additional
            var addi_cmd = AdditionalCommands?.Where(m => m.Attribute.Command.Equals(command) && m.Func != null).FirstOrDefault();
            if (addi_cmd != null)
            {
                return GetCommandDetail(addi_cmd.Attribute, addi_cmd.Settings);
            }

            return null;
        }

        private string GetCommandDetail(TelegramBotCommand attr, Dictionary<string, bool> args)
        {
            var msg = string.Empty;

            // Command name
            msg = msg.AppendNewLine("*Command:*");
            msg = msg.AppendNewLine(attr.Command);

            // Required parameters
            var req = args?.Where(a => !a.Value);
            if (req != null && req.Count() > 0)
            {
                msg = msg.AppendNewLine();
                msg = msg.AppendNewLine("*Required parameter(s):*");
                msg = msg.AppendNewLine(string.Join(" ", req.Select(r => $"-{r.Key}")));
            }

            // Optional parameters
            var opt = args?.Where(a => a.Value);
            if (opt != null && opt.Count() > 0)
            {
                msg = msg.AppendNewLine();
                msg = msg.AppendNewLine("*Optional parameter(s):*");
                msg = msg.AppendNewLine(string.Join(" ", opt.Select(r => $"-{r.Key}")));
            }

            // Description
            msg = msg.AppendNewLine();
            msg = msg.AppendNewLine("*Description:*");
            msg = msg.AppendNewLine(attr.Description);

            // Parameter usage
            if (!string.IsNullOrWhiteSpace(attr.ArgUsage))
            {
                msg = msg.AppendNewLine();
                msg = msg.AppendNewLine("*Parameter usage:*");
                msg = msg.AppendNewLine(attr.ArgUsage);
            }

            return msg;
        }

        private string ExecuteCommand(string input, long chatId)
        {
            input = input?.Trim();
            if (!string.IsNullOrWhiteSpace(input) && input.StartsWith("/"))
            {
                string[] split = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                // Get cmd
                var cmd = split[0].Substring(1).ToLower();

                if (!string.IsNullOrWhiteSpace(cmd))
                {
                    // Get args
                    Dictionary<string, string> args = new Dictionary<string, string>();
                    for (int i = 1; i < split.Length; i++)
                    {
                        var arg = split[i];
                        if (arg.StartsWith("-"))
                        {
                            arg = arg.Substring(1);
                            if (!string.IsNullOrWhiteSpace(arg))
                            {
                                if (i + 1 < split.Length && !split[i + 1].StartsWith("-"))
                                {
                                    args.Add(arg, split[++i]);
                                }
                                else
                                {
                                    args.Add(arg, "true");
                                }
                                continue;
                            }
                        }
                        break;
                    }

                    if (args.Count == 1 && args.ContainsKey("help")) return GetCommandDetail(cmd);

                    Func<string, Task<Message>> sendMessageFunc = (msg) => {
                        return SendTextMessageAsync(chatId, msg);
                    };

                    // Native Method
                    if (Commands.ContainsKey(cmd))
                    {
                        var method = Commands[cmd];
                        // Check argument validity
                        var methodArgs = method.GetParameters();
                        if (methodArgs.All(a =>
                            a.ParameterType == TelegramBotCommand.SendMessageFuncType ||
                            a.ParameterType == typeof(string) && (a.IsOptional || args.ContainsKey(a.Name))
                        ))
                        {
                            // Invoke
                            object[] parameters = new object[methodArgs.Length];
                            for(int i=0; i<methodArgs.Length; i++)
                            {
                                if (methodArgs[i].ParameterType == TelegramBotCommand.SendMessageFuncType)
                                {
                                    parameters[i] = sendMessageFunc;
                                }
                                else if (args.ContainsKey(methodArgs[i].Name))
                                {
                                    parameters[i] = args[methodArgs[i].Name];
                                }
                                else
                                {
                                    parameters[i] = Type.Missing;
                                }
                            }
                            return method.Invoke(this, parameters) as string;
                        }
                    }

                    // Additional Command
                    var addi_cmd = AdditionalCommands?.Where(m => !string.IsNullOrWhiteSpace(m.Attribute.Command) && m.Func != null)
                        .Where(ac => ac.Attribute.Command.Equals(cmd) && ac.Settings?.All(s => s.Value || args.ContainsKey(s.Key)) != false)
                        .FirstOrDefault();
                    if (addi_cmd != null)
                    {
                        // Invoke
                        if (addi_cmd.Settings != null) foreach (var setting in addi_cmd.Settings) if (!args.ContainsKey(setting.Key)) args[setting.Key] = null;
                        return addi_cmd.Func.Invoke(args);
                    }
                }
            }

            return null;
        }
        #endregion

        public void Dispose()
        {
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(2000);
                this.Client.StopReceiving();
                this.Client = null;
            });
        }
    }
}