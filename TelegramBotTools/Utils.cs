﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotTools
{
    public static class Utils
    {
        public static string AppendNewLine(this string original, string newLine = "")
        {
            return original + newLine + Environment.NewLine;
        }
    }
}